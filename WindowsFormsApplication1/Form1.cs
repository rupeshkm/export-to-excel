﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form

    {

        SQLiteConnection con = null;
        
        public Form1()
        {
            InitializeComponent();
           
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            SQLiteConnection.CreateFile("database.db");
            SQLiteConnection con = new SQLiteConnection("data source = database.db");
            con.Open();
            //SQLiteConnection.CreateFile("MyDatabase.sqlite");

            //SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            //m_dbConnection.Open();
            MessageBox.Show(con + " ");


            String createTable = "create table MyTable (id integer, name varchar(20),email varchar(20))";
            SQLiteCommand cmd = con.CreateCommand();
            cmd.CommandText = createTable;

            int i = cmd.ExecuteNonQuery();
            MessageBox.Show(i + "");
            cmd = con.CreateCommand();

            cmd.CommandText = "insert into MyTable (id,name,email) values ('102','rupesh','aa@aa.com')";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "insert into MyTable (id,name,email) values ('103','rupesh',' ')";
            cmd.ExecuteNonQuery();
            cmd = con.CreateCommand();
            SQLiteDataAdapter da = new SQLiteDataAdapter();
            DataTable dt = new DataTable();
            String query = "Select * from MyTable";
            cmd.CommandText = query;
            da.SelectCommand = cmd;
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            //// creating new WorkBook within Excel application
            //Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);


            //// creating new Excelsheet in workbook
            //Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            //// see the excel sheet behind the program
            //app.Visible = true;

            //// get the reference of first sheet. By default its name is Sheet1.
            //// store its reference to worksheet
            //worksheet = workbook.Sheets["Sheet1"];
            //worksheet = workbook.ActiveSheet;

            //// changing the name of active sheet
            //worksheet.Name = "Exported from gridview";

            //for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            //{
            //    worksheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            //}
            //// storing Each row and column value to excel sheet
            //for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            //{
            //    for (int j = 0; j < dataGridView1.Columns.Count; j++)
            //    {
            //        worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
            //    }
            //}


            //// save the application
            //workbook.SaveAs("d:\\output.xls");

            //// Exit from the application
            //app.Quit();

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "export.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridView1, @"c:\export.xls");
                ToCsV(dataGridView1, sfd.FileName); // Here dataGridview1 is your grid view name
            }
        }

        private void ToCsV(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount - 1; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }
    }
}
